# Frontend Test Case

This repo is for doing Frontend Assignment and to submit answer.

## Installation

Clone the repo.

```bash
git clone https://github.com/gaisuke/todoapp.git
```

Install the package

```bash
npm install
```

Run project

```bash
npm start
```

## Tech Stack

- ReactJS
- Redux
- TailwindCSS
- Axios